<?php

/* Class Vehicle toevoegen */
require('vehicleClass.php');

/* Object(en) aanmaken */
$airplane = new Vehicle();
$car = new Vehicle();
$scooter = new Vehicle();

/* Object properties setten */
$airplane->setName("airplane");
$airplane->setMoveBy("air");
$airplane->setFuelType("gasoline");
$airplane->setMaxSpeed(870);

$car->setName("car");
$car->setMoveBy("land");
$car->setFuelType("diesel");
$car->setMaxSpeed(220);

$scooter->setName("scooter");
$scooter->setMoveBy("land");
$scooter->setFuelType("electric");
$scooter->setMaxSpeed(65);

/* Get output */	
echo "<span style='font-size:22px'>";
echo "</br>";
echo $airplane->costs();
echo "</br>";
echo $airplane->arrivesIn();
echo "</br>";
echo "</br>";
echo $car->costs();
echo "</br>";
echo $car->arrivesIn();
echo "</br>";
echo "</br>";
echo $scooter->costs();
echo "</br>";
echo $scooter->arrivesIn()."</span>";


?>
