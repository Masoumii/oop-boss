<?php

class Vehicle{

 /* properties */
 protected $name = null;     // Naam van voertuig
 protected $moveBy = null;   // transport-type
 protected $fuelType = null; //  brandstof-type
 protected $maxSpeed = null; // Snelheid in km/h 
 protected $tripDuration = null; // Reisduur in minuten
 protected $costsPerKm = null; // Reiskosten in euro's
 protected $totalTripCosts = null; // Totale reiskosten in euro's
 protected $isValidTrMethod = null; // Checkt of het een geldige transport methode is
 protected $isValidFlMethod = null; // Checkt of het een geldige brandstof methode is
 protected $gasolinePrice = null; // gasoline prijs per km
 protected $dieselPrice = null; // diesel prijs per km
 protected $electricPrice = null; // electric prijs per km
protected $travelDistance = 1000; // Reisafstand in km

 /* constructor */
 public function __construct(){
    }

    /* Haal huidige brandstofprijzen op uit online database */
    public function getFuelPrices(){
      require_once("db.php");
      $conn = DatabaseConnection::getConnection();
      $q = "SELECT * FROM fuelprices";
      $stmt = $conn->prepare( $q );
      $stmt->execute();

      while($row = $stmt->fetch()){
        $this->gasolinePrice = $row['gasoline_price'];
        $this->dieselPrice = $row['diesel_price'];
        $this->electricPrice = $row['electric_price'];
        }
  }


 /* Class method om te kijken of transport methode mogelijk / toegestaan is */
 public function checkIfValidTr($moveBy){
    if($moveBy === "air" || $moveBy === "sea" || $moveBy === "land"){
       $this->isValidTrMethod = true;
       return $this->isValidTrMethod;
      }
    }

  /* Class method om te kijken of brandstof methode mogelijk / toegestaan is */
 public function checkIfValidFl($fuelType){
    if($fuelType === "gasoline" || $fuelType === "diesel" || $fuelType === "electric"){
       $this->isValidFlMethod = true;
       return $this->isValidFlMethod;
     }
    }


  /* getter voor naam */
  public function getName(){
   return $this->name;
    }

 /* setter voor naam */
  public function setName($name){  
    $this->name = $name;
   }

/* getter voor fuelType */
  public function getfuelType(){
   return $this->fuelType;
    }

 /* setter voor fuelType */
  public function setFuelType($fuelType){
    $this->checkIfValidFl($fuelType);
    if($this->isValidFlMethod===true){
    $this->fuelType = $fuelType;
    }
   }

 /* getter voor moveBy */
  public function getMoveBy(){
   return $this->moveBy;
    }

 /* setter voor moveBy */
  public function setMoveBy($moveBy){
    $this->checkIfValidTr($moveBy);
    if($this->isValidTrMethod===true){
    $this->moveBy = $moveBy;
     }
    }

  /* getter voor maxSpeed */
  public function getMaxSpeed(){
   return $this->maxSpeed;
    }

 /* setter voor maxSpeed */
  public function setMaxSpeed($maxSpeed){

    if($this->moveBy === 'air'){
      $this->maxSpeed = $maxSpeed * 1.5;
    }
    else{
   $this->maxSpeed = $maxSpeed;
   }
  }

    /* getter voor distance */
  public function getTravelDistance(){
   return $this->travelDistance;
   }

  /* Class method om alle mogelijke info in 1 keer op te vragen */  
  public function getAllInfo(){

   $this->checkIfValidTr($this->moveBy);
   $this->checkIfValidFl($this->fuelType);

   if($this->isValidTrMethod===true && $this->isValidFlMethod===true){
       return "Name: {$this->name}, Transportation method: {$this->moveBy}, Fuel-type: {$this->fuelType}, Travel Distance: {$this->travelDistance} km. ";
    }
    else{
         return "The transportation or the fuel method of this vehicle ({$this->name}) is not valid";
       }
    }

  /* Class method om te berekenen hoelang het voertuig onderweg is a.d.h.v. maxSpeed & travelDistance */
  public function arrivesIn(){
        $this->tripDuration = ($this->travelDistance / $this->maxSpeed) * 60; // in minuten

      if($this->tripDuration>60){
        $this->tripDuration = ($this->travelDistance / $this->maxSpeed);
        return "{$this->name} will arrive at the destination within ". round($this->tripDuration) . " hour(s). ";
      }else{
        $this->tripDuration = ($this->travelDistance / $this->maxSpeed) * 60;
      return "{$this->name} will arrive at the destination within ". round($this->tripDuration). " minutes";
      }
    }

    /* Class method om te berekenen wat de reiskosten zijn a.d.h.v. fuelType & travelDistance */
  public function costs(){
      $this->getFuelPrices();
       switch ($this->fuelType) {


         case 'gasoline':
           $this->costsPerKm = $this->gasolinePrice;
           break;
         case 'diesel':
            $this->costsPerKm = $this->dieselPrice;
           break;
         case 'electric':
            $this->costsPerKm = $this->electricPrice;
           break;
         
         default:
            $this->costsPerKm = null;
           break;

        return $this->costsPerKm;
       }


        $this->totalTripCosts = $this->costsPerKm * $this->travelDistance;
      return "Traveling with a {$this->name} over a distance of {$this->travelDistance} km will cost around {$this->totalTripCosts} euro's";

    }

}

?>
